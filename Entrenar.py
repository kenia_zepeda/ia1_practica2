#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime

from FileManagement import File
from Logistic_Regression.Model import Model
from Logistic_Regression.Data import Data
from Logistic_Regression import Plotter
import numpy as np
import os
import json

ONLY_SHOW = False  # Veo si quiero mostrar una imagen del conjunto de datos


def iniciarEntreno():
    entrenar('USAC')
    entrenar('Landivar')
    entrenar('Mariano')
    entrenar('Marroquin')


def entrenar(uni):
    # Cargando conjuntos de datos
    modelos = []
    train_set_x, train_set_y, test_set_x, test_set_y, classes = File.generate_dataset(uni)

    # Definir los conjuntos de datos
    train_set = Data(train_set_x, train_set_y, 255)
    test_set = Data(test_set_x, test_set_y, 255)

    print("Entrenando modelo 1 " + uni + "...............")
    model1 = Model(train_set, test_set, reg=True, alpha=0.001, lam=150, it=5000)
    model1.training()
    modelos.append(model1)

    print("Entrenando modelo 2 " + uni + "...............")
    model2 = Model(train_set, test_set, reg=True, alpha=0.0025, lam=200, it=5000)
    model2.training()
    modelos.append(model2)

    print("Entrenando modelo 3 " + uni + "...............")
    model3 = Model(train_set, test_set, reg=True, alpha=0.005, lam=100, it=5000)
    model3.training()
    modelos.append(model3)

    print("Entrenando modelo 4 " + uni + "...............")
    model4 = Model(train_set, test_set, reg=True, alpha=0.0001, lam=200, it=5000)
    model4.training()
    modelos.append(model4)

    print("Entrenando modelo 5 " + uni + "...............")
    model5 = Model(train_set, test_set, reg=True, alpha=0.00099, lam=200, it=5000)
    model5.training()
    modelos.append(model5)

    modelos = sorted(modelos, key=lambda item: item.train_accuracy, reverse=True)
    guardar(modelos, uni)
    escribirBitacora(modelos, uni)
    return modelos


def escribirBitacora(modelos, uni):
    print("Escribiendo en bitacora")
    fp = os.path.join(os.getcwd(), 'Resultados', uni, 'bitacora.txt')
    f = open(fp, "a")
    tiempo = datetime.now()

    contenido = [
        '--------------ENTRENO----------------\n',
        'Fecha de ejecución: ' + tiempo.date().isoformat() + '\n',
        'Hora de ejecución: ' + tiempo.time().isoformat() + '\n\n',

        '--------------MODELO 1----------------\n',

        'exactitud-entreno: ', str(modelos[0].train_accuracy) + '\n',
        'exactitud-validacion: ', str(modelos[0].test_accuracy) + '\n',
        'nombre: ', 'Modelo1' + '\n',
        'reg: ', str(modelos[0].reg) + '\n',
        'alpha: ', str(modelos[0].alpha) + '\n',
        'lam: ', str(modelos[0].lam) + '\n',
        'iteraciones: ', str(modelos[0].max_iterations) + '\n',

        '--------------MODELO 2----------------\n',

        'exactitud-entreno: ', str(modelos[1].train_accuracy) + '\n',
        'exactitud-validacion: ', str(modelos[1].test_accuracy) + '\n',
        'nombre: ', 'Modelo2' + '\n',
        'reg: ', str(modelos[1].reg) + '\n',
        'alpha: ', str(modelos[1].alpha) + '\n',
        'lam: ', str(modelos[1].lam) + '\n',
        'iteraciones: ', str(modelos[1].max_iterations) + '\n',

        '-------------MODELO 3-----------------\n',

        'exactitud-entreno: ', str(modelos[2].train_accuracy) + '\n',
        'exactitud-validacion: ', str(modelos[2].test_accuracy) + '\n',
        'nombre: ', 'Modelo3' + '\n',
        'reg: ', str(modelos[2].reg) + '\n',
        'alpha: ', str(modelos[2].alpha) + '\n',
        'lam: ', str(modelos[2].lam) + '\n',
        'iteraciones: ', str(modelos[2].max_iterations) + '\n',

        '-------------MODELO 4-----------------\n',

        'exactitud-entreno: ', str(modelos[3].train_accuracy) + '\n',
        'exactitud-validacion: ', str(modelos[3].test_accuracy) + '\n',
        'nombre: ', 'Modelo4' + '\n',
        'reg: ', str(modelos[3].reg) + '\n',
        'alpha: ', str(modelos[3].alpha) + '\n',
        'lam: ', str(modelos[3].lam) + '\n',
        'iteraciones: ', str(modelos[3].max_iterations) + '\n',

        '------------MODELO 5------------------\n',

        'exactitud-entreno: ', str(modelos[4].train_accuracy) + '\n',
        'exactitud-validacion: ', str(modelos[4].test_accuracy) + '\n',
        'nombre: ', 'Modelo5' + '\n',
        'reg: ', str(modelos[4].reg) + '\n',
        'alpha: ', str(modelos[4].alpha) + '\n',
        'lam: ', str(modelos[4].lam) + '\n',
        'iteraciones: ', str(modelos[4].max_iterations) + '\n',
        '-------------FIN-----------------\n',
    ]
    f.writelines(contenido)
    f.close()

    return 0


def guardar(modelos, uni):
    imagen = Plotter.show_Model(modelos)

    fp = os.path.join(os.getcwd(), 'Resultados', uni)
    imagen.savefig(fp + '\\' + uni + '.png')

    info = []
    i = 0
    for mo in modelos:
        obj = {
            'nombre': 'Modelo' + str(i + 1),
            'reg': modelos[i].reg,
            'alpha': modelos[i].alpha,
            'lam': modelos[i].lam,
            'iteraciones': modelos[i].max_iterations,
            'exactitud-entreno': modelos[i].train_accuracy,
            'exactitud-validacion': modelos[i].test_accuracy
        }
        i = i + 1
        info.append(obj)

    with open(fp + '/info.json', 'r+') as ff:
        json.dump(info, ff)

    '''betas = []
    for mo in modelos:
        betas.append(mo.betas)


    for mo in modelos:
        with open(os.getcwd() + '\\Resultados\\' + uni + '\\betas.json', 'r+') as f:
            json.dump(str(mo.betas),f)'''


iniciarEntreno()
