#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import numpy as np
import h5py
import glob
import os
import numpy as np
import h5py
import matplotlib.image as mpimg

source = None


def load_dataset():
    train_dataset = h5py.File('datasets/train_catvnoncat.h5', "r")

    # train_set_x_orig = arreglo de imágenes
    # train_set_y_orig = arreglo de imágenes

    train_set_x_orig = np.array(train_dataset["train_set_x"][:])  # entradas de entrenamiento
    train_set_y_orig = np.array(train_dataset["train_set_y"][:])  # salidas de entrenamiento

    # print('************** train_set_x_orig **************')
    # print(train_set_x_orig)
    # print(type(train_set_x_orig))
    # print('************** train_set_y_orig **************')
    # print(train_set_y_orig)
    # print(len(train_set_y_orig))

    test_dataset = h5py.File('datasets/test_catvnoncat.h5', "r")
    test_set_x_orig = np.array(test_dataset["test_set_x"][:])  # entradas de prueba
    test_set_y_orig = np.array(test_dataset["test_set_y"][:])  # salidas de prueba

    # print('************** test_set_x_orig **************')
    # print(test_set_x_orig)
    # print(len(test_set_x_orig))
    # print('************** test_set_y_orig **************')
    # print(test_set_y_orig) #Arreglo con las respuestas correctas, donde 0 = NO es un gato, 1 = SÍ es un gato
    # print(len(test_set_y_orig))

    # Les aplica reshape, convierte al arreglo en un arreglo de areglos
    train_set_y_orig = train_set_y_orig.reshape((1, train_set_y_orig.shape[0]))
    test_set_y_orig = test_set_y_orig.reshape((1, test_set_y_orig.shape[0]))

    # print('************** train_set_y_orig con reshape**************')
    # print(train_set_y_orig)
    # print(len(train_set_y_orig))
    # print('************** test_set_y_orig con reshape**************')
    # print(test_set_y_orig)
    # print(len(test_set_y_orig))

    # print(type(train_set_x_orig))
    # print(type(train_set_y_orig))
    # print(type(test_set_x_orig))
    # print(type(test_set_y_orig))

    # print(len(train_set_x_orig))
    # print(train_set_x_orig.shape)

    return train_set_x_orig, train_set_y_orig, test_set_x_orig, test_set_y_orig, ['No Gato', 'Gato']


def generate_dataset(uni):
    # train_set_x_orig, train_set_y_orig, test_set_x_orig, test_set_y_orig
    data = []
    classes = [uni, 'No es ' + uni]

    path = 'C:\\Users\\User\\Desktop\\IA\\Laboratorio\\Practica2\\IA1_Practica2\\Imagenes'
    files = os.listdir(path)
    addrs = glob.glob(path + '\\' + uni + '\\*.jpg')
    y = [1 for addr in addrs]
    for namedir in files:
        if namedir != uni:
            auxaddrs = glob.glob(path + '\\' + namedir + '\\*.jpg')
            addrs.extend(auxaddrs)
            y.extend([0 for addr in auxaddrs])

    for i in range(len(addrs)):
        addr = addrs[i]
        img = mpimg.imread(addr)
        data.append([img, y[i]])

    np.random.shuffle(data)
    train = data[0:int(0.8 * len(data))]
    test = data[int(0.8 * len(data)):]

    train_set_x_orig = [data[0] for data in train]
    train_set_y_orig = [data[1] for data in train]

    test_set_x_orig = [data[0] for data in test]
    test_set_y_orig = [data[1] for data in test]

    train_set_x_orig = np.array(train_set_x_orig)
    test_set_x_orig = np.array(test_set_x_orig)
    # Convertir imagenes a un solo arreglo
    train_set_x = train_set_x_orig.reshape(train_set_x_orig.shape[0], -1).T
    test_set_x = test_set_x_orig.reshape(test_set_x_orig.shape[0], -1).T

    # Vean la diferencia de la conversion
    print('Original: ', train_set_x_orig.shape)
    print('Con reshape: ', train_set_x.shape)

    train_set_y_orig = np.array(train_set_y_orig)
    test_set_y_orig = np.array(test_set_y_orig)
    # Convertir array a un solo arreglo
    train_set_y = train_set_y_orig.reshape(train_set_y_orig.shape[0], -1).T
    test_set_y = test_set_y_orig.reshape(test_set_y_orig.shape[0], -1).T

    return train_set_x, train_set_y, test_set_x, test_set_y, classes


def generate_img(addr):
    train_set_x_orig = [mpimg.imread(addr)]
    train_set_x_orig = np.array(train_set_x_orig)
    train_set_x = train_set_x_orig.reshape(train_set_x_orig.shape[0], -1).T

    test_set_y_orig = np.array([1])
    test_set_y = test_set_y_orig.reshape((1, test_set_y_orig.shape[0]))

    print('Original: ', train_set_x_orig.shape)
    print('Con reshape: ', train_set_x.shape)

    return train_set_x, test_set_y





#generate_dataset('USAC')
