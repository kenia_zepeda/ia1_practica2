--------------ENTRENO----------------
Fecha de ejecución: 2020-12-29
Hora de ejecución: 01:05:38.756541

--------------MODELO 1----------------
exactitud-entreno: 96.03399433427762
exactitud-validacion: 92.13483146067416
nombre: Modelo1
reg: True
alpha: 0.001
lam: 100
iteraciones: 10000
--------------MODELO 2----------------
exactitud-entreno: 94.61756373937678
exactitud-validacion: 89.88764044943821
nombre: Modelo2
reg: True
alpha: 0.003
lam: 100
iteraciones: 10000
-------------MODELO 3-----------------
exactitud-entreno: 93.20113314447592
exactitud-validacion: 91.01123595505618
nombre: Modelo3
reg: True
alpha: 0.001
lam: 500
iteraciones: 10000
-------------MODELO 4-----------------
exactitud-entreno: 93.20113314447592
exactitud-validacion: 91.01123595505618
nombre: Modelo4
reg: True
alpha: 0.0005
lam: 500
iteraciones: 10000
------------MODELO 5------------------
exactitud-entreno: 82.71954674220963
exactitud-validacion: 75.28089887640449
nombre: Modelo5
reg: True
alpha: 0.005
lam: 100
iteraciones: 10000
-------------FIN-----------------
--------------ENTRENO----------------
Fecha de ejecución: 2020-12-30
Hora de ejecución: 00:42:39.797836

--------------MODELO 1----------------
exactitud-entreno: 75.92067988668555
exactitud-validacion: 77.52808988764045
nombre: Modelo1
reg: True
alpha: 0.001
lam: 150
iteraciones: 1
--------------MODELO 2----------------
exactitud-entreno: 75.92067988668555
exactitud-validacion: 77.52808988764045
nombre: Modelo2
reg: True
alpha: 0.0025
lam: 200
iteraciones: 1
-------------MODELO 3-----------------
exactitud-entreno: 75.92067988668555
exactitud-validacion: 77.52808988764045
nombre: Modelo3
reg: True
alpha: 0.005
lam: 100
iteraciones: 1
-------------MODELO 4-----------------
exactitud-entreno: 75.92067988668555
exactitud-validacion: 77.52808988764045
nombre: Modelo4
reg: True
alpha: 0.0001
lam: 200
iteraciones: 1
------------MODELO 5------------------
exactitud-entreno: 75.92067988668555
exactitud-validacion: 77.52808988764045
nombre: Modelo5
reg: True
alpha: 0.0009
lam: 200
iteraciones: 1
-------------FIN-----------------
--------------ENTRENO----------------
Fecha de ejecución: 2020-12-30
Hora de ejecución: 00:43:23.874435

--------------MODELO 1----------------
exactitud-entreno: 75.63739376770539
exactitud-validacion: 79.7752808988764
nombre: Modelo1
reg: True
alpha: 0.0025
lam: 200
iteraciones: 1
--------------MODELO 2----------------
exactitud-entreno: 75.63739376770539
exactitud-validacion: 79.7752808988764
nombre: Modelo2
reg: True
alpha: 0.005
lam: 100
iteraciones: 1
-------------MODELO 3-----------------
exactitud-entreno: 75.35410764872522
exactitud-validacion: 79.7752808988764
nombre: Modelo3
reg: True
alpha: 0.001
lam: 150
iteraciones: 1
-------------MODELO 4-----------------
exactitud-entreno: 75.35410764872522
exactitud-validacion: 79.7752808988764
nombre: Modelo4
reg: True
alpha: 0.0001
lam: 200
iteraciones: 1
------------MODELO 5------------------
exactitud-entreno: 75.35410764872522
exactitud-validacion: 79.7752808988764
nombre: Modelo5
reg: True
alpha: 0.00099
lam: 200
iteraciones: 1
-------------FIN-----------------
--------------ENTRENO----------------
Fecha de ejecución: 2020-12-30
Hora de ejecución: 01:40:21.683048

--------------MODELO 1----------------
exactitud-entreno: 96.3172804532578
exactitud-validacion: 89.88764044943821
nombre: Modelo1
reg: True
alpha: 0.001
lam: 150
iteraciones: 5000
--------------MODELO 2----------------
exactitud-entreno: 95.1841359773371
exactitud-validacion: 89.88764044943821
nombre: Modelo2
reg: True
alpha: 0.00099
lam: 200
iteraciones: 5000
-------------MODELO 3-----------------
exactitud-entreno: 92.35127478753542
exactitud-validacion: 86.51685393258427
nombre: Modelo3
reg: True
alpha: 0.0001
lam: 200
iteraciones: 5000
-------------MODELO 4-----------------
exactitud-entreno: 84.70254957507082
exactitud-validacion: 79.7752808988764
nombre: Modelo4
reg: True
alpha: 0.005
lam: 100
iteraciones: 5000
------------MODELO 5------------------
exactitud-entreno: 82.43626062322946
exactitud-validacion: 76.40449438202248
nombre: Modelo5
reg: True
alpha: 0.0025
lam: 200
iteraciones: 5000
-------------FIN-----------------
