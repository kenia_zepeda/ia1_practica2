from flask import Flask, render_template, request
from FileManagement import File
from Logistic_Regression.Model import Model
from Logistic_Regression.Data import Data
from Logistic_Regression import Plotter
import numpy as np
import os
import json

app = Flask(__name__)

modeloUsac = Model
modeloLandivar = Model
modeloMarroquin = Model
modeloMariano = Model


@app.route('/')
def hello_world():
	 iniciarModelos()
    return render_template("index.html", a1=[], a2=[], a3="CREANDO MODELOS, DURACION APROXIMADA 30 MIN", len=0)


@app.route('/entrenar',methods=['POST', 'GET'])
def entrenar():
    iniciarModelos()
    return "finalizo entreno"

@app.route('/analizar', methods=['POST', 'GET'])
def analizar():
    path = 'C:\\Users\\User\\Desktop\\IA\\Laboratorio\\Practica2\\IA1_Practica2\\static\\Predecir\\'
    marianoAcierto = 0
    usacAcierto = 0
    landivarAcierto = 0
    marroquinAcierto = 0
    marianoIntentos = 0.0001
    usacIntentos = 0.0001
    landivarIntentos = 0.0001
    marroquinIntentos = 0.0001
    imagenes = []
    resultados = []
    resultado = "Ninguno"
    usacP = 0
    marianoP = 0
    marroquinP = 0
    landivarP = 0

    f = request.files.getlist('imagenes')
    for file in f:
        img = path + file.filename
        imgname = file.filename.lower()
        imgname = imgname.split("_")[0]

        if imgname == "usac":
            usacIntentos += 1
        if imgname == "mariano":
            marianoIntentos += 1
        if imgname == "landivar":
            landivarIntentos += 1
        if imgname == "marroquin":
            marroquinIntentos += 1

        usacRes, marianoRes, landivarRes, marroquinRes = predecir(img)
        if usacRes:
            resultado = "USAC"
            if imgname == "usac":
                usacAcierto += 1
        if marianoRes:
            resultado = "Mariano"
            if imgname == "mariano":
                marianoAcierto += 1
        if landivarRes:
            resultado = "Landivar"
            if imgname== "landivar":
                landivarAcierto += 1
        if marroquinRes:
            resultado = "Marroquin"
            if imgname == "marroquin":
                marroquinAcierto += 1
        resultados.append(resultado)
        imagenes.append(file.filename)

    usacP = round((usacAcierto / usacIntentos) * 100, 2)
    marianoP = round((marianoAcierto / marianoIntentos) * 100, 2)
    marroquinP = round((marroquinAcierto / marroquinIntentos) * 100, 2)
    landivarP = round((landivarAcierto / landivarIntentos) * 100, 2)
    porcentajes = "\nUSAC: " + str(usacP) + "\nMARIANO: " + str(marianoP) + "\nLANDIVAR: " + str(landivarP) + "\nMARROQUIN: " + str(marroquinP)
    if (len(imagenes) > 5):
        return render_template("index.html", a1=[], a2=[], a3=porcentajes, len=0)
    else:
        return render_template("index.html", a1=resultados, a2=imagenes, a3=porcentajes, len=len(resultados))


def iniciarModelos():
    global modeloUsac
    global modeloLandivar
    global modeloMarroquin
    global modeloMariano

    print("Creando modelo USAC...............")
    train_set_x, train_set_y, test_set_x, test_set_y, classes = File.generate_dataset("USAC")
    train_set = Data(train_set_x, train_set_y, 255)
    test_set = Data(test_set_x, test_set_y, 255)
    modeloUsac = Model(train_set, test_set, reg=True, alpha=0.001, lam=100, it=5000)
    modeloUsac.training()
    print("Creando modelo Landivar...............")
    train_set_x, train_set_y, test_set_x, test_set_y, classes = File.generate_dataset("Landivar")
    train_set = Data(train_set_x, train_set_y, 255)
    test_set = Data(test_set_x, test_set_y, 255)
    modeloLandivar = Model(train_set, test_set, reg=True, alpha=0.001, lam=100, it=5000)
    modeloLandivar.training()

    print("Creando modelo Mariano...............")
    train_set_x, train_set_y, test_set_x, test_set_y, classes = File.generate_dataset("Mariano")
    train_set = Data(train_set_x, train_set_y, 255)
    test_set = Data(test_set_x, test_set_y, 255)
    modeloMariano = Model(train_set, test_set, reg=True, alpha=0.001, lam=100, it=5000)
    modeloMariano.training()

    print("Creando modelo Marroquin...............")
    train_set_x, train_set_y, test_set_x, test_set_y, classes = File.generate_dataset("Marroquin")
    train_set = Data(train_set_x, train_set_y, 255)
    test_set = Data(test_set_x, test_set_y, 255)
    modeloMarroquin = Model(train_set, test_set, reg=True, alpha=0.001, lam=100, it=5000)
    modeloMarroquin.training()

    return render_template("index.html", a1=[], a2=[], a3="SE TERMINÓ DE CREAR LOS MODELOS", len=0)




def predecir(img):
    global modeloUsac
    global modeloLandivar
    global modeloMarroquin
    global modeloMariano

    train_set_x, train_set_y = File.generate_img(img)
    train_set = Data(train_set_x, train_set_y, 255)
    resUsac = modeloUsac.predict(train_set.x)
    resMariano = modeloMariano.predict(train_set.x)
    resLandivar = modeloLandivar.predict(train_set.x)
    resMarroquin = modeloMarroquin.predict(train_set.x)

    usacAccuracy = 100 - np.mean(np.abs(resUsac - train_set.y)) * 100
    usacRes = True if resUsac == 1 else False

    marianoAccuracy = 100 - np.mean(np.abs(resMariano - train_set.y)) * 100
    marianoRes = True if resMariano == 1 else False

    landivarAccuracy = 100 - np.mean(np.abs(resLandivar - train_set.y)) * 100
    landivarRes = True if resLandivar == 1 else False

    marroquinAccuracy = 100 - np.mean(np.abs(resMarroquin - train_set.y)) * 100
    marroquinRes = True if resMarroquin == 1 else False

    return usacRes, marianoRes, landivarRes, marroquinRes


if __name__ == '__main__':
    app.run()
